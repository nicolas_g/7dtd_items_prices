﻿Imports KFPTextBox

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ShopItemsPriceListview
    Inherits System.Windows.Forms.UserControl

    'UserControl1 remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ShopItemsPriceListview))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.TextBox23 = New KFPTextBox.KFP_TextBox.KFP_TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.PnlItem = New System.Windows.Forms.Panel()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TxtNbrItem = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.LblName = New System.Windows.Forms.Label()
        Me.PicItem = New System.Windows.Forms.PictureBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.PanelSrch = New System.Windows.Forms.Panel()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.LblPages = New System.Windows.Forms.Label()
        Me.PnlToolTip = New System.Windows.Forms.Panel()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.LblitemNameInfo = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.PnltoolTipIn = New System.Windows.Forms.Panel()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.KfP_TextBox1 = New KFPTextBox.KFP_TextBox.KFP_TextBox()
        Me.LblStamina_usage = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.LblDamageBlock = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.LblDamageEntity = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.LblMaterial = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.LblInfoRange = New System.Windows.Forms.Label()
        Me.LblInfoSN = New System.Windows.Forms.Label()
        Me.LblInfoRT = New System.Windows.Forms.Label()
        Me.LblInfoGroup = New System.Windows.Forms.Label()
        Me.LblinfoW = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.PnlItem.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicItem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelSrch.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.PnlToolTip.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.PnltoolTipIn.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel1.BackColor = System.Drawing.Color.Black
        Me.Panel1.Controls.Add(Me.PictureBox5)
        Me.Panel1.Controls.Add(Me.PictureBox4)
        Me.Panel1.Controls.Add(Me.Label31)
        Me.Panel1.Controls.Add(Me.Panel6)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.PnlItem)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.FlowLayoutPanel1)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.PictureBox15)
        Me.Panel1.Controls.Add(Me.PanelSrch)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.PnlToolTip)
        Me.Panel1.ForeColor = System.Drawing.Color.Red
        Me.Panel1.Location = New System.Drawing.Point(1, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(728, 194)
        Me.Panel1.TabIndex = 0
        '
        'PictureBox5
        '
        Me.PictureBox5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox5.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(666, 146)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(16, 16)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox5.TabIndex = 199
        Me.PictureBox5.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox4.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(640, 176)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(68, 15)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 198
        Me.PictureBox4.TabStop = False
        '
        'Label31
        '
        Me.Label31.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.Red
        Me.Label31.Location = New System.Drawing.Point(653, 162)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(44, 12)
        Me.Label31.TabIndex = 197
        Me.Label31.Text = "Ketchu13"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Panel6.Controls.Add(Me.TextBox23)
        Me.Panel6.Location = New System.Drawing.Point(47, 5)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(147, 18)
        Me.Panel6.TabIndex = 126
        '
        'TextBox23
        '
        Me.TextBox23.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox23.BackColor = System.Drawing.Color.Black
        Me.TextBox23.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox23.ForeColor = System.Drawing.Color.White
        Me.TextBox23.Location = New System.Drawing.Point(1, 1)
        Me.TextBox23.MinimumSize = New System.Drawing.Size(0, 16)
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.ShortcutsEnabled = False
        Me.TextBox23.Size = New System.Drawing.Size(145, 16)
        Me.TextBox23.TabIndex = 120
        Me.TextBox23.TabStop = False
        Me.TextBox23.Text = "44Magnum"
        Me.TextBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.BackColor = System.Drawing.Color.Black
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.ForeColor = System.Drawing.Color.Lime
        Me.TextBox1.Location = New System.Drawing.Point(559, 23)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(41, 20)
        Me.TextBox1.TabIndex = 5
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBox1.Visible = False
        '
        'PnlItem
        '
        Me.PnlItem.BackColor = System.Drawing.Color.Red
        Me.PnlItem.Controls.Add(Me.TextBox2)
        Me.PnlItem.Controls.Add(Me.TxtNbrItem)
        Me.PnlItem.Controls.Add(Me.Label29)
        Me.PnlItem.Controls.Add(Me.Label10)
        Me.PnlItem.Controls.Add(Me.PictureBox1)
        Me.PnlItem.Controls.Add(Me.LblName)
        Me.PnlItem.Controls.Add(Me.PicItem)
        Me.PnlItem.Location = New System.Drawing.Point(8, 40)
        Me.PnlItem.Name = "PnlItem"
        Me.PnlItem.Size = New System.Drawing.Size(146, 139)
        Me.PnlItem.TabIndex = 1
        Me.PnlItem.Visible = False
        '
        'TextBox2
        '
        Me.TextBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox2.BackColor = System.Drawing.Color.Black
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.Color.Fuchsia
        Me.TextBox2.Location = New System.Drawing.Point(69, 116)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(76, 22)
        Me.TextBox2.TabIndex = 197
        Me.TextBox2.Text = "0"
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TxtNbrItem
        '
        Me.TxtNbrItem.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TxtNbrItem.BackColor = System.Drawing.Color.Black
        Me.TxtNbrItem.ForeColor = System.Drawing.Color.Lime
        Me.TxtNbrItem.Location = New System.Drawing.Point(66, 102)
        Me.TxtNbrItem.Name = "TxtNbrItem"
        Me.TxtNbrItem.Size = New System.Drawing.Size(79, 13)
        Me.TxtNbrItem.TabIndex = 18
        Me.TxtNbrItem.Text = "100"
        Me.TxtNbrItem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label29
        '
        Me.Label29.AutoEllipsis = True
        Me.Label29.BackColor = System.Drawing.Color.Black
        Me.Label29.ForeColor = System.Drawing.Color.Lime
        Me.Label29.Location = New System.Drawing.Point(1, 102)
        Me.Label29.Margin = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(68, 13)
        Me.Label29.TabIndex = 125
        Me.Label29.Text = "Prix moyen:"
        '
        'Label10
        '
        Me.Label10.AutoEllipsis = True
        Me.Label10.BackColor = System.Drawing.Color.Black
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(1, 116)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(68, 22)
        Me.Label10.TabIndex = 124
        Me.Label10.Text = "Votre prix:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Black
        Me.PictureBox1.Image = Global._7dtd_Items_Prices.My.Resources.Resources.export
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(15, 13)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 122
        Me.PictureBox1.TabStop = False
        '
        'LblName
        '
        Me.LblName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblName.AutoEllipsis = True
        Me.LblName.BackColor = System.Drawing.Color.Black
        Me.LblName.ForeColor = System.Drawing.Color.White
        Me.LblName.Location = New System.Drawing.Point(1, 84)
        Me.LblName.Name = "LblName"
        Me.LblName.Size = New System.Drawing.Size(144, 17)
        Me.LblName.TabIndex = 121
        Me.LblName.Text = "ItemName"
        Me.LblName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PicItem
        '
        Me.PicItem.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PicItem.BackColor = System.Drawing.Color.Black
        Me.PicItem.BackgroundImage = Global._7dtd_Items_Prices.My.Resources.Resources.fgrid1
        Me.PicItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PicItem.Image = Global._7dtd_Items_Prices.My.Resources.Resources.NoPic
        Me.PicItem.Location = New System.Drawing.Point(1, 1)
        Me.PicItem.Name = "PicItem"
        Me.PicItem.Size = New System.Drawing.Size(144, 82)
        Me.PicItem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PicItem.TabIndex = 0
        Me.PicItem.TabStop = False
        '
        'Label9
        '
        Me.Label9.AutoEllipsis = True
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.Color.Red
        Me.Label9.Location = New System.Drawing.Point(3, 8)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(44, 13)
        Me.Label9.TabIndex = 120
        Me.Label9.Text = "Search:"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Black
        Me.FlowLayoutPanel1.BackgroundImage = Global._7dtd_Items_Prices.My.Resources.Resources.dkhome
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 29)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(2)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(485, 165)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'ComboBox1
        '
        Me.ComboBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ComboBox1.BackColor = System.Drawing.Color.Black
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(197, 4)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(147, 21)
        Me.ComboBox1.TabIndex = 115
        Me.ComboBox1.TabStop = False
        '
        'PictureBox15
        '
        Me.PictureBox15.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox15.Image = Global._7dtd_Items_Prices.My.Resources.Resources.Cancel
        Me.PictureBox15.Location = New System.Drawing.Point(350, 6)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(16, 16)
        Me.PictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox15.TabIndex = 191
        Me.PictureBox15.TabStop = False
        '
        'PanelSrch
        '
        Me.PanelSrch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelSrch.BackgroundImage = Global._7dtd_Items_Prices.My.Resources.Resources.fgrid1
        Me.PanelSrch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PanelSrch.Controls.Add(Me.Label12)
        Me.PanelSrch.Controls.Add(Me.Panel7)
        Me.PanelSrch.Controls.Add(Me.Label24)
        Me.PanelSrch.Controls.Add(Me.Label25)
        Me.PanelSrch.Controls.Add(Me.Label26)
        Me.PanelSrch.Controls.Add(Me.Label27)
        Me.PanelSrch.Controls.Add(Me.Label28)
        Me.PanelSrch.Location = New System.Drawing.Point(345, 1)
        Me.PanelSrch.Name = "PanelSrch"
        Me.PanelSrch.Size = New System.Drawing.Size(383, 25)
        Me.PanelSrch.TabIndex = 196
        Me.PanelSrch.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(26, 6)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(68, 13)
        Me.Label12.TabIndex = 11
        Me.Label12.Text = "Found Items:"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Transparent
        Me.Panel7.Controls.Add(Me.Label19)
        Me.Panel7.Controls.Add(Me.Label21)
        Me.Panel7.Controls.Add(Me.Label23)
        Me.Panel7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Panel7.Location = New System.Drawing.Point(195, 6)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(79, 15)
        Me.Panel7.TabIndex = 10
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(34, 1)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(12, 13)
        Me.Label19.TabIndex = 5
        Me.Label19.Text = "/"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(2, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(36, 15)
        Me.Label21.TabIndex = 4
        Me.Label21.Text = "1"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Location = New System.Drawing.Point(42, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(36, 15)
        Me.Label23.TabIndex = 6
        Me.Label23.Text = "1"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label24.Location = New System.Drawing.Point(298, 6)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(19, 13)
        Me.Label24.TabIndex = 5
        Me.Label24.Text = ">>"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label25.Location = New System.Drawing.Point(279, 6)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(13, 13)
        Me.Label25.TabIndex = 6
        Me.Label25.Text = ">"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.Label26.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label26.Location = New System.Drawing.Point(176, 6)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(13, 13)
        Me.Label26.TabIndex = 7
        Me.Label26.Text = "<"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label27.Location = New System.Drawing.Point(151, 6)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(19, 13)
        Me.Label27.TabIndex = 8
        Me.Label27.Text = "<<"
        '
        'Label28
        '
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label28.Location = New System.Drawing.Point(97, 5)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(54, 14)
        Me.Label28.TabIndex = 9
        Me.Label28.Text = "1"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(401, 7)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(65, 13)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Items count:"
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(665, 7)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(19, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = ">>"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(646, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(13, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = ">"
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(543, 7)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(13, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "<"
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(518, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(19, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "<<"
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.Location = New System.Drawing.Point(472, 6)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 14)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "1"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.LblPages)
        Me.Panel3.Location = New System.Drawing.Point(562, 7)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(79, 15)
        Me.Panel3.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(34, 1)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(12, 13)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "/"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 15)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "1"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblPages
        '
        Me.LblPages.BackColor = System.Drawing.Color.Black
        Me.LblPages.Location = New System.Drawing.Point(42, 0)
        Me.LblPages.Name = "LblPages"
        Me.LblPages.Size = New System.Drawing.Size(36, 15)
        Me.LblPages.TabIndex = 6
        Me.LblPages.Text = "1"
        Me.LblPages.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PnlToolTip
        '
        Me.PnlToolTip.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PnlToolTip.BackColor = System.Drawing.Color.DarkRed
        Me.PnlToolTip.Controls.Add(Me.PictureBox2)
        Me.PnlToolTip.Controls.Add(Me.LblitemNameInfo)
        Me.PnlToolTip.Controls.Add(Me.Panel4)
        Me.PnlToolTip.Location = New System.Drawing.Point(486, 26)
        Me.PnlToolTip.Name = "PnlToolTip"
        Me.PnlToolTip.Size = New System.Drawing.Size(243, 169)
        Me.PnlToolTip.TabIndex = 195
        '
        'PictureBox2
        '
        Me.PictureBox2.BackgroundImage = Global._7dtd_Items_Prices.My.Resources.Resources.fgrid1
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox2.Location = New System.Drawing.Point(6, 5)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(47, 36)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 1
        Me.PictureBox2.TabStop = False
        '
        'LblitemNameInfo
        '
        Me.LblitemNameInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblitemNameInfo.BackColor = System.Drawing.Color.Black
        Me.LblitemNameInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblitemNameInfo.ForeColor = System.Drawing.Color.Red
        Me.LblitemNameInfo.Location = New System.Drawing.Point(2, 1)
        Me.LblitemNameInfo.Name = "LblitemNameInfo"
        Me.LblitemNameInfo.Size = New System.Drawing.Size(222, 16)
        Me.LblitemNameInfo.TabIndex = 0
        Me.LblitemNameInfo.Text = "Item Infos"
        Me.LblitemNameInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel4
        '
        Me.Panel4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel4.AutoScroll = True
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Panel4.Controls.Add(Me.PnltoolTipIn)
        Me.Panel4.Location = New System.Drawing.Point(1, 1)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(239, 166)
        Me.Panel4.TabIndex = 194
        '
        'PnltoolTipIn
        '
        Me.PnltoolTipIn.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PnltoolTipIn.AutoScroll = True
        Me.PnltoolTipIn.AutoScrollMargin = New System.Drawing.Size(25, 25)
        Me.PnltoolTipIn.AutoSize = True
        Me.PnltoolTipIn.BackColor = System.Drawing.Color.Black
        Me.PnltoolTipIn.BackgroundImage = Global._7dtd_Items_Prices.My.Resources.Resources.dkowner
        Me.PnltoolTipIn.Controls.Add(Me.Label30)
        Me.PnltoolTipIn.Controls.Add(Me.KfP_TextBox1)
        Me.PnltoolTipIn.Controls.Add(Me.LblStamina_usage)
        Me.PnltoolTipIn.Controls.Add(Me.Label22)
        Me.PnltoolTipIn.Controls.Add(Me.LblDamageBlock)
        Me.PnltoolTipIn.Controls.Add(Me.Label20)
        Me.PnltoolTipIn.Controls.Add(Me.LblDamageEntity)
        Me.PnltoolTipIn.Controls.Add(Me.Label13)
        Me.PnltoolTipIn.Controls.Add(Me.LblMaterial)
        Me.PnltoolTipIn.Controls.Add(Me.Label11)
        Me.PnltoolTipIn.Controls.Add(Me.LblInfoRange)
        Me.PnltoolTipIn.Controls.Add(Me.LblInfoSN)
        Me.PnltoolTipIn.Controls.Add(Me.LblInfoRT)
        Me.PnltoolTipIn.Controls.Add(Me.LblInfoGroup)
        Me.PnltoolTipIn.Controls.Add(Me.LblinfoW)
        Me.PnltoolTipIn.Controls.Add(Me.Label14)
        Me.PnltoolTipIn.Controls.Add(Me.Label15)
        Me.PnltoolTipIn.Controls.Add(Me.Label16)
        Me.PnltoolTipIn.Controls.Add(Me.Label17)
        Me.PnltoolTipIn.Controls.Add(Me.Label18)
        Me.PnltoolTipIn.Location = New System.Drawing.Point(0, 0)
        Me.PnltoolTipIn.Name = "PnltoolTipIn"
        Me.PnltoolTipIn.Size = New System.Drawing.Size(239, 180)
        Me.PnltoolTipIn.TabIndex = 193
        '
        'Label30
        '
        Me.Label30.AutoEllipsis = True
        Me.Label30.BackColor = System.Drawing.Color.Black
        Me.Label30.ForeColor = System.Drawing.Color.White
        Me.Label30.Location = New System.Drawing.Point(64, 23)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(57, 17)
        Me.Label30.TabIndex = 126
        Me.Label30.Text = "Votre prix:"
        '
        'KfP_TextBox1
        '
        Me.KfP_TextBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.KfP_TextBox1.BackColor = System.Drawing.Color.Black
        Me.KfP_TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.KfP_TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KfP_TextBox1.ForeColor = System.Drawing.Color.Fuchsia
        Me.KfP_TextBox1.Location = New System.Drawing.Point(136, 21)
        Me.KfP_TextBox1.MinimumSize = New System.Drawing.Size(0, 16)
        Me.KfP_TextBox1.Name = "KfP_TextBox1"
        Me.KfP_TextBox1.ReadOnly = True
        Me.KfP_TextBox1.ShortcutsEnabled = False
        Me.KfP_TextBox1.Size = New System.Drawing.Size(83, 17)
        Me.KfP_TextBox1.TabIndex = 125
        Me.KfP_TextBox1.TabStop = False
        Me.KfP_TextBox1.Text = "N/C"
        Me.KfP_TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblStamina_usage
        '
        Me.LblStamina_usage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblStamina_usage.AutoEllipsis = True
        Me.LblStamina_usage.BackColor = System.Drawing.Color.Transparent
        Me.LblStamina_usage.ForeColor = System.Drawing.Color.White
        Me.LblStamina_usage.Location = New System.Drawing.Point(90, 154)
        Me.LblStamina_usage.Name = "LblStamina_usage"
        Me.LblStamina_usage.Size = New System.Drawing.Size(74, 13)
        Me.LblStamina_usage.TabIndex = 17
        Me.LblStamina_usage.Text = "NC"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(1, 140)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(83, 13)
        Me.Label22.TabIndex = 16
        Me.Label22.Text = "Stamina_usage:"
        '
        'LblDamageBlock
        '
        Me.LblDamageBlock.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblDamageBlock.AutoEllipsis = True
        Me.LblDamageBlock.BackColor = System.Drawing.Color.Transparent
        Me.LblDamageBlock.ForeColor = System.Drawing.Color.White
        Me.LblDamageBlock.Location = New System.Drawing.Point(90, 140)
        Me.LblDamageBlock.Name = "LblDamageBlock"
        Me.LblDamageBlock.Size = New System.Drawing.Size(74, 13)
        Me.LblDamageBlock.TabIndex = 15
        Me.LblDamageBlock.Text = "NC"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(1, 167)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(77, 13)
        Me.Label20.TabIndex = 14
        Me.Label20.Text = "DamageBlock:"
        '
        'LblDamageEntity
        '
        Me.LblDamageEntity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblDamageEntity.AutoEllipsis = True
        Me.LblDamageEntity.BackColor = System.Drawing.Color.Transparent
        Me.LblDamageEntity.ForeColor = System.Drawing.Color.White
        Me.LblDamageEntity.Location = New System.Drawing.Point(90, 126)
        Me.LblDamageEntity.Name = "LblDamageEntity"
        Me.LblDamageEntity.Size = New System.Drawing.Size(74, 13)
        Me.LblDamageEntity.TabIndex = 13
        Me.LblDamageEntity.Text = "NC"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(1, 154)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(76, 13)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "DamageEntity:"
        '
        'LblMaterial
        '
        Me.LblMaterial.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblMaterial.AutoEllipsis = True
        Me.LblMaterial.BackColor = System.Drawing.Color.Transparent
        Me.LblMaterial.ForeColor = System.Drawing.Color.White
        Me.LblMaterial.Location = New System.Drawing.Point(90, 58)
        Me.LblMaterial.Name = "LblMaterial"
        Me.LblMaterial.Size = New System.Drawing.Size(74, 13)
        Me.LblMaterial.TabIndex = 8
        Me.LblMaterial.Text = "NC"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(1, 58)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(47, 13)
        Me.Label11.TabIndex = 7
        Me.Label11.Text = "Material:"
        '
        'LblInfoRange
        '
        Me.LblInfoRange.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblInfoRange.AutoEllipsis = True
        Me.LblInfoRange.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoRange.ForeColor = System.Drawing.Color.White
        Me.LblInfoRange.Location = New System.Drawing.Point(90, 167)
        Me.LblInfoRange.Name = "LblInfoRange"
        Me.LblInfoRange.Size = New System.Drawing.Size(74, 13)
        Me.LblInfoRange.TabIndex = 6
        Me.LblInfoRange.Text = "NC"
        '
        'LblInfoSN
        '
        Me.LblInfoSN.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblInfoSN.AutoEllipsis = True
        Me.LblInfoSN.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoSN.ForeColor = System.Drawing.Color.White
        Me.LblInfoSN.Location = New System.Drawing.Point(90, 112)
        Me.LblInfoSN.Name = "LblInfoSN"
        Me.LblInfoSN.Size = New System.Drawing.Size(74, 13)
        Me.LblInfoSN.TabIndex = 5
        Me.LblInfoSN.Text = "NC"
        '
        'LblInfoRT
        '
        Me.LblInfoRT.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblInfoRT.AutoEllipsis = True
        Me.LblInfoRT.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoRT.ForeColor = System.Drawing.Color.White
        Me.LblInfoRT.Location = New System.Drawing.Point(90, 99)
        Me.LblInfoRT.Name = "LblInfoRT"
        Me.LblInfoRT.Size = New System.Drawing.Size(74, 13)
        Me.LblInfoRT.TabIndex = 4
        Me.LblInfoRT.Text = "NC"
        '
        'LblInfoGroup
        '
        Me.LblInfoGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblInfoGroup.AutoEllipsis = True
        Me.LblInfoGroup.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoGroup.ForeColor = System.Drawing.Color.White
        Me.LblInfoGroup.Location = New System.Drawing.Point(90, 86)
        Me.LblInfoGroup.Name = "LblInfoGroup"
        Me.LblInfoGroup.Size = New System.Drawing.Size(74, 13)
        Me.LblInfoGroup.TabIndex = 3
        Me.LblInfoGroup.Text = "NC"
        '
        'LblinfoW
        '
        Me.LblinfoW.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblinfoW.AutoEllipsis = True
        Me.LblinfoW.BackColor = System.Drawing.Color.Transparent
        Me.LblinfoW.ForeColor = System.Drawing.Color.White
        Me.LblinfoW.Location = New System.Drawing.Point(90, 71)
        Me.LblinfoW.Name = "LblinfoW"
        Me.LblinfoW.Size = New System.Drawing.Size(74, 13)
        Me.LblinfoW.TabIndex = 2
        Me.LblinfoW.Text = "1"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(1, 126)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(35, 13)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Class:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(1, 99)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(73, 13)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "Reapair Tools"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(2, 72)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(44, 13)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "Weight:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(1, 112)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(70, 13)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Stacknumber"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(1, 86)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(39, 13)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "Group:"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Red
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(730, 196)
        Me.Panel2.TabIndex = 1
        '
        'ShopItemsPriceListview
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.Red
        Me.Controls.Add(Me.Panel2)
        Me.DoubleBuffered = True
        Me.Name = "ShopItemsPriceListview"
        Me.Size = New System.Drawing.Size(730, 196)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.PnlItem.ResumeLayout(False)
        Me.PnlItem.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicItem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelSrch.ResumeLayout(False)
        Me.PanelSrch.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.PnlToolTip.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.PnltoolTipIn.ResumeLayout(False)
        Me.PnltoolTipIn.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents TextBox23 As KFPTextBox.KFP_TextBox.KFP_TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents PnlItem As System.Windows.Forms.Panel
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents LblName As System.Windows.Forms.Label
    Friend WithEvents PicItem As System.Windows.Forms.PictureBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents PnlToolTip As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents LblitemNameInfo As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents PnltoolTipIn As System.Windows.Forms.Panel
    Friend WithEvents LblStamina_usage As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents LblDamageBlock As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents LblDamageEntity As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents LblMaterial As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents LblInfoRange As System.Windows.Forms.Label
    Friend WithEvents LblInfoSN As System.Windows.Forms.Label
    Friend WithEvents LblInfoRT As System.Windows.Forms.Label
    Friend WithEvents LblInfoGroup As System.Windows.Forms.Label
    Friend WithEvents LblinfoW As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents PictureBox15 As System.Windows.Forms.PictureBox
    Friend WithEvents PanelSrch As System.Windows.Forms.Panel
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents LblPages As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents TxtNbrItem As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents KfP_TextBox1 As KFPTextBox.KFP_TextBox.KFP_TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Label31 As System.Windows.Forms.Label

End Class

﻿Imports System.ComponentModel

<Serializable()> _
Public Class KFPFormCtrl
    Private Shared m_format As DataFormats.Format
    Private m_ctrlName As String
    Private m_partialName As String
    Private m_propertyList As New Hashtable()

    Shared Sub New()
        m_format = DataFormats.GetFormat(GetType(KFPFormCtrl).FullName)
    End Sub

    Public Shared ReadOnly Property Format() As DataFormats.Format
        Get
            Return m_format
        End Get
    End Property
    Public Property CtrlName() As String
        Get
            Return m_ctrlName
        End Get
        Set(ByVal value As String)
            m_ctrlName = value
        End Set
    End Property

    Public Property PartialName() As String
        Get
            Return m_partialName
        End Get
        Set(ByVal value As String)
            m_partialName = value
        End Set
    End Property

    Public ReadOnly Property PropertyList() As Hashtable
        Get
            Return m_propertyList
        End Get
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal ctrl As Control)
        CtrlName = ctrl.[GetType]().Name
        PartialName = ctrl.[GetType]().[Namespace]

        Dim properties As PropertyDescriptorCollection = TypeDescriptor.GetProperties(ctrl)

        For Each myProperty As PropertyDescriptor In properties
            Try
                If myProperty.PropertyType.IsSerializable Then
                    m_propertyList.Add(myProperty.Name, myProperty.GetValue(ctrl))
                End If
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try
        Next
    End Sub
End Class
﻿Imports _7dtd_Items_Prices.KFPBoutiques
Imports System.Runtime.Serialization
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Net

Public Class Form1

    Private m_iconsPath As String = Application.StartupPath & "\ItemIcons\"
    Public m_itemslist As ShopItemsList
    Private Updater As String
    Private version As String = "0.1.5"
    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            Dim formatter As IFormatter = New BinaryFormatter()
            Dim stream As Stream = New FileStream("MyFile_" & TextBox7.Text & ".bin", FileMode.Create, FileAccess.Write, FileShare.None)
            formatter.Serialize(stream, Me.m_itemslist)
            stream.Close()
        Catch ex As Exception
            '   MsgBox(ex.Message)
        End Try
        Try
            Dim formatter As IFormatter = New BinaryFormatter()
            Dim stream As Stream = New FileStream("MyFile.bin", FileMode.Create, FileAccess.Write, FileShare.None)
            formatter.Serialize(stream, Me.m_itemslist)
            stream.Close()
        Catch ex As Exception
            '  MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CheckUpdate(Me)
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        GetSavedPrices(Me)
    End Sub

    Function GetAllShopItems() As String
        Dim str As String = Nothing
        For i As Integer = 0 To Me.m_itemslist.Count - 1
            Dim ShpI As ShopItem = Me.m_itemslist(i)
            If Not ShpI Is Nothing Then
                str &= ShpI.Name & "," & ShpI.CurrentPrice & vbCrLf

            End If

        Next
        Return "shopitems=" & str & "zombies," & TextBox2.Text & vbCrLf & _
            "minutes," & TextBox1.Text & vbCrLf & _
            "tptofriend," & TextBox4.Text & vbCrLf & _
            "tpfriend," & TextBox3.Text & vbCrLf & _
            "suicide," & TextBox5.Text & vbCrLf
    End Function
    Sub GetSavedPrices(ByRef frm As Form1)
        Dim Str As System.IO.Stream
        Dim srRead As System.IO.StreamReader
        Try
            ' make a Web request
            Dim wc As New WebClient
            wc.Headers("content-type") = "application/x-www-form-urlencoded"
            Dim ass As String = GetAllShopItems()
            Dim response As String = wc.UploadString("http://www.ketchu-free-party.fr/7D2D/Application/ZBot_7dtd-ItemsPrice/itemsPrices.php", ass)
            '      Dim req As System.Net.WebRequest = System.Net.WebRequest.Create("http://www.ketchu-free-party.fr/7D2D/Application/ZBot_7dtd-ItemsPrice/itemsPrices.php?shopitems=" & ass)
            '      Dim resp As System.Net.WebResponse = req.GetResponse
            '     Str = resp.GetResponseStream
            ' str    srRead = New System.IO.StreamReader(Str)
            ' read all the text 
            Dim resultat As String = response ' srRead.ReadToEnd
            'TODO check version value smaller or not
            MsgBox(resultat)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            ' Close Stream and StreamReader when done
            Try
                srRead.Close()
                Str.Close()
            Catch ex As Exception

            End Try

        End Try
    End Sub
    Sub CheckUpdate(ByRef frm As Form1)
        Dim Str As System.IO.Stream
        Dim srRead As System.IO.StreamReader
        Try
            ' make a Web request
            Dim req As System.Net.WebRequest = System.Net.WebRequest.Create("http://www.ketchu-free-party.fr/7D2D/Application/ZBot_7dtd-ItemsPrice/version.php")
            Dim resp As System.Net.WebResponse = req.GetResponse
            Str = resp.GetResponseStream
            srRead = New System.IO.StreamReader(Str)
            ' read all the text 
            Dim resultat As String = srRead.ReadToEnd
            'TODO check version value smaller or not
            If frm.version.Equals(resultat) = False AndAlso frm.Updater = True Then
                frm.Updater = False
                Dim rslt As MsgBoxResult = MsgBox("Une mise à jour est disponible..." & vbCrLf & _
                                                  vbCrLf & _
                                                  "Version disponible: " & resultat & vbCrLf & _
                                                  "Version du Zbot installée: " & frm.version & vbCrLf & _
                                                  vbCrLf & _
                                                  "Voulez-vous là télécharger ?", MsgBoxStyle.YesNo, "Update Time !")
                If rslt = MsgBoxResult.Yes Then
                    Process.Start("https://www.dropbox.com/s/lpkk7cnqmes8v9q/Debug.rar?dl=0")
                Else
                    If resultat.Contains("B") = True Then
                        frm.Close()
                    End If
                End If
                frm.Updater = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            ' Close Stream and StreamReader when done
            Try
                srRead.Close()
                Str.Close()
            Catch ex As Exception

            End Try

        End Try
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim stream As Stream
        Try
            Dim formatter As IFormatter = New BinaryFormatter()
            stream = New FileStream("MyFile.bin", FileMode.Open, FileAccess.Read, FileShare.Read)
            Me.m_itemslist = DirectCast(formatter.Deserialize(stream), ShopItemsList)
            stream.Close()
        Catch ex As Exception
            m_itemslist = New ShopItemsList(m_iconsPath)
            Try
                stream.Close()
            Catch ex1 As Exception

            End Try

        End Try
        Me.ShopItemsPriceListview1.IconsPath = Me.m_iconsPath
        Me.ShopItemsPriceListview1.ShopItemsList = Me.m_itemslist
        Me.ShopItemsPriceListview1.NbrItems = Me.m_itemslist.Count
        If m_itemslist.Count > 0 Then
            ShopItemsPriceListview1.DisplayItems()
        End If
        Panel3.Visible = False
    End Sub


End Class

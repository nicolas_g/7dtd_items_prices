﻿'Imports System.IO
'Imports System.Xml

Imports System.Drawing
Imports System.Xml
Imports System.IO

Namespace KFPBoutiques
    <Serializable> _
    Public Class ShopItemsList
        Inherits List(Of ShopItem)
        Private m_IconsPath As String = Nothing
        Dim dico2 As Dictionary(Of String, Color)
        'Dim dico2 As Dictionary(Of String, Color)
#Region "Properties"
        Public ReadOnly Property GetItemInfosByKeyAndName(ByVal key As String, ByVal name As String) As String
            Get
                Dim pair As KeyValuePair(Of String, String)
                Dim user As ShopItem = Me.GetShopItemByName(name)
                If Not user Is Nothing Then
                    For Each pair In user.Properties()
                        If pair.Key.ToString.Equals(key) = True Then
                            If Not pair.Value Is Nothing Then
                                Return pair.Value.ToString()
                            End If
                        End If
                    Next
                End If
                Return Nothing
            End Get
        End Property
#End Region
        Public Property IconsPath() As String
            Get
                Return Me.m_IconsPath
            End Get
            Set(value As String)
                Me.m_IconsPath = value
            End Set
        End Property
#Region "New"
        Public Sub New(ByRef iconpath As String)
            Me.IconsPath = iconpath
            ReadItemsXml()

        End Sub
        Public Sub New()

        End Sub
#End Region
#Region "Functions"
        Public Function GetShopItemByName(ByVal name As String) As ShopItem
            Return Me.Find(Function(t As ShopItem) t.Name = name)
        End Function
        Public Function ReadItemsXml() As Boolean
            '    Dim list As List(Of Item) = New List(Of Item)()
            '  Dim rdn As New Random
            dico2 = New Dictionary(Of String, Color)
            dico2.Add("Ammo/Weapons", Color.FromArgb(255, 255, 0, 0))
            dico2.Add("Tools/Traps", Color.FromArgb(255, 255, 0, 0))
            dico2.Add("Building", Color.FromArgb(255, 255, 164, 18))
            dico2.Add("Resources", Color.FromArgb(255, 255, 164, 18))
            dico2.Add("Forging/Molds", Color.FromArgb(255, 255, 164, 18))
            dico2.Add("Decor", Color.FromArgb(255, 255, 0, 255))
            dico2.Add("Medicine", Color.FromArgb(255, 255, 255, 0))
            dico2.Add("Chemicals", Color.FromArgb(255, 255, 255, 0))
            dico2.Add("Food/Cooking", Color.FromArgb(255, 0, 255, 0))
            dico2.Add("Clothing", Color.FromArgb(255, 145, 76, 6))
            dico2.Add("Miscellaneous", Color.FromArgb(255, 0, 0, 255))
            dico2.Add("Special Items", Color.FromArgb(255, 184, 6, 255))
            Try
                If File.Exists(Application.StartupPath & "\items.xml") Then
                    Me.Clear()
                    Dim xmlReaderSettings As XmlReaderSettings = New XmlReaderSettings()
                    xmlReaderSettings.IgnoreComments = True
                    Using xmlReader As XmlReader = xmlReader.Create(Application.StartupPath & "\items.xml", xmlReaderSettings)
                        Dim xmlDocument As XmlDocument = New XmlDocument()
                        xmlDocument.Load(xmlReader)
                        If xmlDocument.ChildNodes.Count = 0 Then
                            Throw New Exception("No element <items> found!")
                        End If

                        Dim xmlNode As XmlNode = xmlDocument.SelectNodes("items")(0)
                        For Each xmlNode2 As XmlNode In xmlNode.ChildNodes
                            ' MsgBox(xmlNode2.InnerText)
                            If xmlNode2.NodeType = XmlNodeType.Element AndAlso xmlNode2.Name.Equals("item") Then
                                Dim xmlElement2 As XmlElement = CType(xmlNode2, XmlElement)
                                Dim item As ShopItem
                                If xmlElement2.HasAttribute("name") Then
                                    Dim atrb As String = xmlElement2.GetAttribute("name")
                                    item = New ShopItem(atrb)
                                    Me.Add(item)
                                    If xmlElement2.HasAttribute("id") Then
                                        item.Id = xmlElement2.GetAttribute("id")
                                    End If
                                    For Each xmlNode3 As XmlNode In xmlElement2.ChildNodes
                                        Try
                                            If xmlNode3.NodeType = XmlNodeType.Element AndAlso xmlNode3.Name.Equals("property") Then
                                                Dim xmlelement As XmlElement = CType(xmlNode3, XmlElement)
                                                Dim cptf As Integer = xmlelement.Attributes.Count
                                                If cptf <= 0 Then
                                                    Exit For
                                                End If
                                                For c As Integer = 0 To cptf - 1
                                                    If xmlelement.Attributes(c).Name.ToLower.Equals("name") = True Then
                                                        Dim PropName As String = xmlelement.Attributes(c).InnerText
                                                        Select Case True
                                                            Case PropName.ToLower.Equals("fuelvalue") = True
                                                                item.FuelValue = Integer.Parse(xmlelement.Attributes(c + 1).InnerText)

                                                            Case PropName.ToLower.Equals("material") = True
                                                                item.Material = xmlelement.Attributes(c + 1).InnerText

                                                            Case PropName.ToLower.Equals("stacknumber") = True
                                                                item.MaxStack = Integer.Parse(xmlelement.Attributes(c + 1).InnerText)

                                                            Case PropName.ToLower.Equals("weight") = True
                                                                item.Weight = xmlelement.Attributes(c + 1).InnerText

                                                            Case PropName.ToLower.Equals("repairtools") = True
                                                                item.RepairTools = xmlelement.Attributes(c + 1).InnerText

                                                            Case PropName.ToLower.Equals("class") = True
                                                                item._Class = xmlelement.Attributes(c + 1).InnerText

                                                            Case PropName.ToLower.Equals("group") = True
                                                                item.Group = Split(xmlelement.Attributes(c + 1).InnerText, ",")(0)
                                                                If Not item.Group Is Nothing AndAlso Len(item.Group) > 3 Then
                                                                    item.Color = dico2(item.Group)
                                                                Else
                                                                    item.Color = Nothing
                                                                End If


                                                            Case PropName.ToLower.Equals("range") = True
                                                                item.Range = xmlelement.Attributes(c + 1).InnerText


                                                        End Select
                                                        If item.RepairTools = Nothing Then
                                                            item.RepairTools = "None"
                                                        End If
                                                        If item.Group = Nothing Then
                                                            item.Group = "None"""
                                                        End If
                                                    Else
                                                        'Dim PropName2 As String = xmlelement.Attributes(c).InnerText
                                                        For Each xmlNodez As XmlNode In xmlelement.ChildNodes
                                                            Dim xmlelementz As XmlElement = CType(xmlNodez, XmlElement)
                                                            Dim cptz As Integer = xmlelementz.Attributes.Count
                                                            If cptz <= 0 Then
                                                                Exit For
                                                            End If
                                                            For z As Integer = 0 To cptz - 1
                                                                If xmlelementz.Attributes(c).Name.ToLower.Equals("name") = True Then
                                                                    Dim PropName3 As String = xmlelementz.Attributes(c).InnerText
                                                                    Select Case True
                                                                        Case PropName3.ToLower.Equals("range") = True
                                                                            item.Range = xmlelementz.Attributes(c + 1).InnerText
                                                                        Case PropName3.ToLower.Equals("DamageEntity") = True
                                                                            item.DamageEntity = xmlelementz.Attributes(c + 1).InnerText
                                                                        Case PropName3.ToLower.Equals("DamageBlock") = True
                                                                            item.DamageBlock = xmlelementz.Attributes(c + 1).InnerText
                                                                        Case PropName3.ToLower.Equals("Stamina_usage") = True
                                                                            item.Stamina_usage = xmlelementz.Attributes(c + 1).InnerText
                                                                        Case PropName3.ToLower.Equals("class") = True
                                                                            item._Class = xmlelementz.Attributes(c + 1).InnerText
                                                                    End Select
                                                                End If


                                                            Next
                                                        Next

                                                    End If
                                                Next
                                            End If
                                        Catch ex As Exception
                                            MsgBox(ex.Message)
                                        End Try
                                    Next
                                    '  list.Add(item)
                                Else

                                End If

                            End If
                        Next
                    End Using
                End If
                '  MsgBox(List.ToString)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            Try
                If File.Exists(Application.StartupPath & "\blocks.xml") Then

                    Dim xmlReaderSettings As XmlReaderSettings = New XmlReaderSettings()
                    xmlReaderSettings.IgnoreComments = True
                    Using xmlReader As XmlReader = xmlReader.Create(Application.StartupPath & "\blocks.xml", xmlReaderSettings)
                        Dim xmlDocument As XmlDocument = New XmlDocument()
                        xmlDocument.Load(xmlReader)
                        If xmlDocument.ChildNodes.Count = 0 Then
                            Throw New Exception("No element <items> found!")
                        End If

                        Dim xmlNode As XmlNode = xmlDocument.SelectNodes("blocks")(0)
                        For Each xmlNode2 As XmlNode In xmlNode.ChildNodes
                            ' MsgBox(xmlNode2.InnerText)
                            If xmlNode2.NodeType = XmlNodeType.Element AndAlso xmlNode2.Name.Equals("block") Then
                                Dim xmlElement2 As XmlElement = CType(xmlNode2, XmlElement)
                                Dim item As ShopItem
                                If xmlElement2.HasAttribute("name") Then
                                    Dim atrb As String = xmlElement2.GetAttribute("name")
                                    item = Me.GetShopItemByName(atrb)
                                    If item Is Nothing Then
                                        item = New ShopItem(atrb)
                                        Me.Add(item)
                                    End If
                                    If xmlElement2.HasAttribute("id") Then
                                        ' item.Id = xmlElement2.GetAttribute("id")
                                    End If
                                    For Each xmlNode3 As XmlNode In xmlElement2.ChildNodes
                                        Try
                                            If xmlNode3.NodeType = XmlNodeType.Element AndAlso xmlNode3.Name.Equals("property") Then
                                                Dim xmlelement As XmlElement = CType(xmlNode3, XmlElement)
                                                Dim cptf As Integer = xmlelement.Attributes.Count
                                                If cptf <= 0 Then
                                                    Exit For
                                                End If
                                                For c As Integer = 0 To cptf - 1
                                                    If xmlelement.Attributes(c).Name.ToLower.Equals("name") = True Then
                                                        Dim PropName As String = xmlelement.Attributes(c).InnerText
                                                        Select Case True
                                                            Case PropName.ToLower.Equals("fuelvalue") = True
                                                                item.FuelValue = Integer.Parse(xmlelement.Attributes(c + 1).InnerText)

                                                            Case PropName.ToLower.Equals("material") = True
                                                                item.Material = xmlelement.Attributes(c + 1).InnerText

                                                            Case PropName.ToLower.Equals("stacknumber") = True
                                                                item.MaxStack = xmlelement.Attributes(c + 1).InnerText
                                                                'Case PropName.ToLower.Equals("sounddestroy") = True
                                                                '    item.sounddestroy = Integer.Parse(xmlelement.Attributes(c + 1).InnerText)

                                                            Case PropName.ToLower.Equals("weight") = True
                                                                item.Weight = xmlelement.Attributes(c + 1).InnerText

                                                            Case PropName.ToLower.Equals("repairtools") = True
                                                                item.RepairTools = xmlelement.Attributes(c + 1).InnerText

                                                            Case PropName.ToLower.Equals("class") = True
                                                                item._Class = xmlelement.Attributes(c + 1).InnerText

                                                            Case PropName.ToLower.Equals("group") = True
                                                                item.Group = xmlelement.Attributes(c + 1).InnerText


                                                        End Select
                                                    End If
                                                Next
                                            End If
                                        Catch ex As Exception
                                            MsgBox(ex.Message)
                                        End Try
                                    Next
                                    '  list.Add(item)
                                Else

                                End If

                            End If
                        Next
                    End Using
                End If
                '  MsgBox(List.ToString)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            If Directory.Exists(Me.m_IconsPath) Then
                Dim Chemin As String = Me.m_IconsPath
                Dim sFiles() As String = Directory.GetFiles(Chemin)
                Dim nbfiles As Integer = Directory.GetFiles(Chemin).Length()
                Dim i As Integer

                For i = 0 To nbfiles - 1
                    Dim pathfile As String = sFiles(i)
                    '/ Console.WriteLine("pathfile " & pathfile)
                    Dim NomFichier As String = pathfile.Remove(0, InStrRev(pathfile, "\", -1))
                    Dim ee As Integer = InStrRev(NomFichier, ".")
                    If ee > 0 Then
                        Dim ext As String = Mid(NomFichier, ee)
                        Dim fichier As String = Mid(NomFichier, 1, ee - 1)
                        If ext.Equals(".png") = True Then
                            If IO.File.Exists(pathfile) Then
                                Dim itm As Item = Me.GetShopItemByName(fichier)
                                If Not itm Is Nothing Then
                                    itm.Icons = pathfile
                                Else
                                    itm = New Item(fichier, pathfile, "500")
                                    Me.Add(itm)
                                End If
                            End If
                        End If
                    End If
                Next
            End If
            Me.Sort(Function(i1 As Item, i2 As Item) i1.Name.CompareTo(i2.Name))
            Return False

        End Function
#End Region
    End Class
End Namespace

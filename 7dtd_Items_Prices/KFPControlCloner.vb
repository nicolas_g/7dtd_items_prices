﻿Imports System.Reflection
Imports System.ComponentModel

Public Class KFPControlCloner
    Public Sub New()
    End Sub

    Public Shared Function CreateControl(ByVal ctrlName As String, ByVal partialName As String) As Control
        Try
            Dim ctrl As Control
            Select Case ctrlName
                Case "Label"
                    ctrl = New Label()
                    Exit Select
                Case "Panel"
                    ctrl = New Panel()
                    Exit Select
                Case "TextBox"
                    ctrl = New TextBox()
                    Exit Select
                Case "PictureBox"
                    ctrl = New PictureBox()
                    Exit Select
                Case "ListView"
                    ctrl = New ListView()
                    Exit Select
                Case "ComboBox"
                    ctrl = New ComboBox()
                    Exit Select
                Case "Button"
                    ctrl = New Button()
                    Exit Select
                Case "CheckBox"
                    ctrl = New CheckBox()
                    Exit Select
                Case "MonthCalender"
                    ctrl = New MonthCalendar()
                    Exit Select
                Case "DateTimePicker"
                    ctrl = New DateTimePicker()
                    Exit Select
                Case Else
                    Dim controlAsm As Assembly = Assembly.Load(partialName)
                    Dim controlType As Type = controlAsm.[GetType](Convert.ToString(partialName & Convert.ToString(".")) & ctrlName)
                    ctrl = DirectCast(Activator.CreateInstance(controlType), Control)
                    Exit Select
            End Select

            Return ctrl
        Catch ex As Exception
            System.Diagnostics.Trace.WriteLine("create control failed:" + ex.Message)
            Return New Control()
        End Try
    End Function

    Public Shared Sub SetControlProperties(ByVal ctrl As Control, ByVal propertyList As Hashtable)
        Dim properties As PropertyDescriptorCollection = TypeDescriptor.GetProperties(ctrl)

        For Each myProperty As PropertyDescriptor In properties
            If propertyList.Contains(myProperty.Name) Then
                Dim obj As [Object] = propertyList(myProperty.Name)
                Try
                    myProperty.SetValue(ctrl, obj)
                Catch ex As Exception
                    Console.WriteLine(ex.Message)
                End Try
            End If
        Next

    End Sub

    Public Shared Function CloneCtrl(ByVal ctrl As Control) As Control
        Dim cbCtrl As New KFPFormCtrl(ctrl)
        Dim newCtrl As Control = KFPControlCloner.CreateControl(cbCtrl.CtrlName, cbCtrl.PartialName)
        KFPControlCloner.SetControlProperties(newCtrl, cbCtrl.PropertyList)
        Return newCtrl
    End Function
End Class

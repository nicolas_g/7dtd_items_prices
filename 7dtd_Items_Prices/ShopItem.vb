﻿'Imports System.Drawing
Namespace KFPBoutiques
    <Serializable> _
    Public Class ShopItem
        Inherits Item
        Private m_IsOnPromo As Boolean
        Private m_SoldePerc As String
        Private m_NormalPrice As Integer
        Private m_CurrentPrice As Integer
        Private m_StackInShop As Integer
        Private m_MaxBuyQT As Integer
        Private m_MinBuyQt As Integer
        Private m_soldedTime As Integer
#Region "Properties"
        ''' <summary>
        ''' Description goes here
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property IsOnPromo() As Boolean
            Get
                Return Me.m_IsOnPromo
            End Get
            Set(ByVal value As Boolean)
                Me.m_IsOnPromo = value
            End Set
        End Property
        ''' <summary>
        ''' Description goes here
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property SoldePerc() As String
            Get
                Return Me.m_SoldePerc
            End Get
            Set(ByVal value As String)
                Me.m_SoldePerc = value
            End Set
        End Property
        ''' <summary>
        ''' Description goes here
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property NormalPrice() As Integer
            Get
                Return Me.m_NormalPrice
            End Get
            Set(ByVal value As Integer)
                Me.m_NormalPrice = value
            End Set
        End Property
        ''' <summary>
        ''' Description goes here
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property CurrentPrice() As Integer
            Get
                Return Me.m_CurrentPrice
            End Get
            Set(ByVal value As Integer)
                Me.m_CurrentPrice = value
            End Set
        End Property
        ''' <summary>
        ''' Description goes here
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property StackInShop() As Integer
            Get
                Return Me.m_StackInShop
            End Get
            Set(ByVal value As Integer)
                Me.m_StackInShop = value
            End Set
        End Property
        ''' <summary>
        ''' Description goes here
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property MaxBuyQT() As Integer
            Get
                Return Me.m_MaxBuyQT
            End Get
            Set(ByVal value As Integer)
                Me.m_MaxBuyQT = value
            End Set
        End Property
        ''' <summary>
        ''' Description goes here
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property MinBuyQt() As Integer
            Get
                Return Me.m_MinBuyQt
            End Get
            Set(ByVal value As Integer)
                Me.m_MinBuyQt = value
            End Set
        End Property
        ''' <summary>
        ''' Description goes here
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property SoldedTime() As Integer
            Get
                Return Me.m_soldedTime
            End Get
            Set(ByVal value As Integer)
                Me.m_soldedTime = value
            End Set
        End Property

#End Region
#Region "New"
        Public Sub New(ByVal name As String)
            Me.Name = name
        End Sub
#End Region
    End Class
End Namespace
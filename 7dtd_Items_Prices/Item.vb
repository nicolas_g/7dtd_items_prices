﻿Imports System.Reflection
Imports System.Drawing

<Serializable()> _
Public Class Item
    Private m_Name As String = Nothing
    Private m_Color As Color = Nothing
    Private m_Group As String = Nothing
    Private m_Id As String = "0"
    Private m_MaxStack As String = "500"
    Private m_CoastValue As String = Nothing
    Private m_RepairTools As String = Nothing
    Private m_Class As String = Nothing
    Private m_Range As String = Nothing
    Private m_Weight As String = Nothing
    Private m_EquipSlot As String = Nothing
    Private m_CustomIcon As String = Nothing
    Private m_CustomIconTint As String = Nothing
    Private m_CritChance As String = Nothing
    Private m_FuelValue As Integer = 0
    Private m_NbrItem As String = Nothing
    Private m_Material As String = Nothing
    Private m_IsGivable As String = "True"
    Private m_DamageEntity As String = Nothing
    Private m_DamageBlock As String = Nothing
    Private m_Stamina_usage As String = Nothing
    Private m_Icons As String = ""
#Region "Properties"
    ''' <summary>
    ''' Description goes here
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Stamina_usage() As String
        Get
            Return Me.m_Stamina_usage
        End Get
        Set(ByVal value As String)
            Me.m_Stamina_usage = value
        End Set
    End Property
    Public Property IsGivable() As String
        Get
            Return Me.m_IsGivable
        End Get
        Set(ByVal value As String)
            Me.m_IsGivable = value
        End Set
    End Property
    Public Property DamageBlock() As String
        Get
            Return Me.m_DamageBlock
        End Get
        Set(ByVal value As String)
            Me.m_DamageBlock = value
        End Set
    End Property
    Public Property DamageEntity() As String
        Get
            Return Me.m_DamageEntity
        End Get
        Set(ByVal value As String)
            Me.m_DamageEntity = value
        End Set
    End Property
    Public Property CoastValue() As String
        Get
            Return m_CoastValue
        End Get
        Set(ByVal value As String)
            m_CoastValue = value
        End Set
    End Property
    Public Property _Class() As String
        Get
            Return Me.m_Class
        End Get
        Set(ByVal value As String)
            Me.m_Class = value
        End Set
    End Property
    Public Property Range() As String
        Get
            Return Me.m_Range
        End Get
        Set(ByVal value As String)
            Me.m_Range = value
        End Set
    End Property
    Public Property CritChance() As String
        Get
            Return Me.m_CritChance
        End Get
        Set(ByVal value As String)
            Me.m_CritChance = value
        End Set
    End Property
    Public Property Weight() As String
        Get
            Return Me.m_Weight
        End Get
        Set(ByVal value As String)
            Me.m_Weight = value
        End Set
    End Property
    Public Property EquipSlot() As String
        Get
            Return Me.m_EquipSlot
        End Get
        Set(ByVal value As String)
            Me.m_EquipSlot = value
        End Set
    End Property
    Public Property RepairTools() As String
        Get
            Return Me.m_RepairTools
        End Get
        Set(ByVal value As String)
            Me.m_RepairTools = value
        End Set
    End Property
    ''' <summary>
    ''' Description goes here
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Id() As String
        Get
            Return m_Id
        End Get
        Set(ByVal value As String)
            m_Id = value
        End Set
    End Property
    ''' <summary>
    ''' Description goes here
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(ByVal value As String)
            m_Name = value
        End Set
    End Property
    ''' <summary>
    ''' Description goes here
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Color() As Color
        Get
            Return m_Color
        End Get
        Set(ByVal value As Color)
            m_Color = value
        End Set
    End Property
    ''' <summary>
    ''' Description goes here
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Group() As String
        Get
            Return m_Group
        End Get
        Set(ByVal value As String)
            m_Group = value
        End Set
    End Property

    Public Property FuelValue() As Integer
        Get
            Return Me.m_FuelValue
        End Get
        Set(ByVal value As Integer)
            Me.m_FuelValue = CInt(value)
        End Set
    End Property

    Public Property Material() As String
        Get
            Return Me.m_Material
        End Get
        Set(ByVal value As String)
            Me.m_Material = value

        End Set
    End Property

    ''' <summary>
    ''' Description goes here
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Icons() As String
        Get
            Return m_Icons
        End Get
        Set(ByVal value As String)
            m_Icons = value
        End Set
    End Property
    ''' <summary>
    ''' Description goes here
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property MaxStack() As String
        Get
            Return m_MaxStack
        End Get
        Set(ByVal value As String)
            m_MaxStack = value
        End Set
    End Property
    ''' <summary>
    ''' Description goes here
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property NbrItem() As String
        Get
            Return m_NbrItem
        End Get
        Set(ByVal value As String)
            m_NbrItem = value
        End Set
    End Property
    ''' <summary>
    ''' Description goes here
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Properties() As Dictionary(Of String, String)

        Get
            Dim propd() As PropertyInfo = Me.[GetType]().GetProperties()
            Dim dd As New Dictionary(Of String, String)

            For c As Integer = 0 To UBound(propd) - 1
                If propd(c).Name.Equals("Properties") = False Then
                    dd.Add(propd(c).Name, Properties(propd(c).Name))
                End If
            Next
            Return dd
        End Get
    End Property
#End Region
#Region "New"
    Public Sub New(ByVal name As String, ByVal iconPath As String, ByVal maxStack As String)
        Me.Name = name
        Me.Icons = iconPath
        Me.MaxStack = maxStack
    End Sub
    Public Sub New(ByVal name As String)
        Me.Name = name
    End Sub
    Public Sub New()

    End Sub
    Public Sub New(ByVal name As String, ByVal iconPath As String)
        Me.Name = name
        Me.Icons = iconPath
    End Sub
#End Region

    Public Sub Dispose()
        Me.Dispose()
    End Sub
End Class

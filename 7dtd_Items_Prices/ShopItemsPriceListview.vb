﻿Imports System.Drawing
Imports System.Windows.Forms
Imports _7dtd_Items_Prices.KFPBoutiques
Imports System.Text.RegularExpressions

Public Class ShopItemsPriceListview

    '  <Description> _
    Private number As Integer = 0
    Private KFPListItems(1000) As String
    Private numberItems As Integer = 0
    Private expand As Boolean = False
    Private SlideSpeed As Integer = 3
    Private m_IconsPath As String = Nothing
    Private m_itemsList As ShopItemsList
    Private m_NbrItems As Integer = 0

    Public Event ErrorEvent As ErrorEventEventHandler
    Public Event NewEvent As NewEventEventHandler

    Public Delegate Sub ErrorEventEventHandler(ByVal num As String)
    Public Delegate Sub NewEventEventHandler(ByVal num As String)


    Public Event ItemDoubleClick As ItemDoubleClickHandler
    Public Event ItemMouseUp As MouseUpHandler
    Public Event ItemKeyUp As KeyUpHandler

    Public Delegate Sub ItemDoubleClickHandler(ByVal itemName As String, ByVal itemCount As String)
    Public Delegate Sub MouseUpHandler(ByVal num As String)
    Public Delegate Sub KeyUpHandler(ByVal num As String)

    Public Sub New()
        InitializeComponent()

    End Sub

#Region "Properties"
    <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
    Public Shadows ReadOnly Property BorderStyle As String
        Get
            Return "KFPStyle"
        End Get
    End Property

    Public Property Items() As String()
        Get
            Dim lll As String()
            lll = Nothing
            lll = (KFPListItems)
            numberItems = UBound(KFPListItems)
            Return lll
        End Get
        Set(value As String())
            'KFPListItems.Clear()
            KFPListItems = value
            numberItems = UBound(KFPListItems)
        End Set
    End Property
    Public Property IconsPath() As String
        Get
            Return Me.m_IconsPath
        End Get
        Set(value As String)
            Me.m_IconsPath = value
        End Set
    End Property
    Public Property ShopItemsList() As ShopItemsList
        Get
            Return Me.m_itemsList
        End Get
        Set(value As ShopItemsList)
            Me.m_itemsList = value
            If Not value Is Nothing Then
                ComboBox1.Items.Clear()
                For Each itm As ShopItem In value
                    ComboBox1.Items.Add(itm.Name)
                    'kComboBox1.AddItem(itm.Name)
                Next
            End If

        End Set
    End Property
    Public Property Items(ByVal i As Integer) As String
        Get
            numberItems = UBound(KFPListItems)
            Return KFPListItems(i)
        End Get
        Set(value As String)
            'KFPListItems.Clear()
            KFPListItems(i) = value
            numberItems = UBound(KFPListItems)
        End Set
    End Property
    Public Overrides Property MinimumSize() As Size
        Get
            Return New Size(730, 159)
        End Get
        Set(value As Size)

        End Set

    End Property

    Public Property FlowBackColor() As Color
        Get
            Return FlowLayoutPanel1.BackColor
        End Get
        Set(value As Color)
            FlowLayoutPanel1.BackColor = value
            ' Me.ForeColor = col
        End Set
    End Property
    Public Property ItemBackColor() As Color
        Get
            Return PnlItem.BackColor
        End Get
        Set(value As Color)
            PnlItem.BackColor = value
            ' Me.ForeColor = col
        End Set
    End Property
    Public Property ItemNameBackColor() As Color
        Get
            Return LblName.BackColor
        End Get
        Set(value As Color)
            LblName.BackColor = value
            ' Me.ForeColor = col
        End Set
    End Property
    Public Property ItemNameForeColor() As Color
        Get
            Return LblName.ForeColor
        End Get
        Set(value As Color)
            LblName.ForeColor = value
            ' Me.ForeColor = col
        End Set
    End Property

    Public Property ItemNameNbrBackColor() As Color
        Get
            Return TxtNbrItem.BackColor
        End Get
        Set(value As Color)
            TxtNbrItem.BackColor = value
        End Set
    End Property
    Public Property ItemNameNbrForeColor() As Color
        Get
            Return TxtNbrItem.ForeColor
        End Get
        Set(value As Color)
            TxtNbrItem.ForeColor = value
        End Set
    End Property
    Public Property IWidth() As Integer 'ByVal str As Integer)
        Get
            Return Panel2.Width
        End Get
        Set(value As Integer)
            Panel2.Width = value 'str
        End Set
    End Property
    Public Overrides Property ForeColor() As Color
        Get
            Return Panel1.ForeColor
        End Get
        Set(value As Color)
            Panel1.ForeColor = value
        End Set
    End Property
    Public Property ILV_BackColor() As Color 'ByVal col As Color)
        Get
            Return Panel1.BackColor
        End Get
        Set(value As Color)
            Panel1.BackColor = value ' col
        End Set
    End Property

    Public Property BorderColor() As Color 'ByVal col As Color)
        Get
            Return Panel2.BackColor
        End Get
        Set(value As Color)
            Panel2.BackColor = value
            Panel2.BackColor = value
            Panel2.BackColor = value ' col
        End Set
    End Property
    Public Property NbrItems() As Integer 'ByVal col As Color)
        Get
            Return Me.m_NbrItems
        End Get
        Set(value As Integer)
            Me.m_NbrItems = value ' col
            Me.Label6.Text = value
        End Set
    End Property

#End Region

    Public Sub RaiseErrorEvent(ByVal httpMethod As String)
        RaiseEvent ErrorEvent(httpMethod)
    End Sub
    Public Sub RaiseNewEvent(ByVal httpMethod As String)
        RaiseEvent NewEvent(httpMethod)
    End Sub


    Public Sub Picitem_DoubleClick(ByVal itemName As String, ByVal itemCount As String)
        RaiseEvent ItemDoubleClick(itemName, itemCount)
    End Sub
    Public Sub Picitem_MouseUp(ByVal httpMethod As String)
        RaiseEvent ItemMouseUp(httpMethod)
    End Sub
    Public Sub Picitem_KeyUp(ByVal httpMethod As String)
        RaiseEvent ItemKeyUp(httpMethod)
    End Sub

    Public Function GetItemsCountInOneCol() As String
        Dim w1 As Integer = Me.FlowLayoutPanel1.Height
        Dim w2 As Integer = Me.PnlItem.Height + 6
        Dim nbr As Integer = Math.Floor(w1 / w2)
        Return nbr
    End Function
    Public Function GetItemsCountInOneLine() As String
        Dim w1 As Integer = Me.FlowLayoutPanel1.Width
        Dim w2 As Integer = Me.PnlItem.Width + 6
        Dim nbr As Integer = Math.Floor(w1 / w2)
        Return nbr
    End Function
    Public Sub RemoveAllControl()
        Me.FlowLayoutPanel1.Controls.Clear()
    End Sub
    Private Sub Me_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Panel2.Width = Me.Width
        Panel2.Height = Me.Height
        Panel1.Width = Panel2.Width - 2
        Panel1.Height = Panel2.Height - 2
        Me.Refresh()
        Me.Label5.Text = 1
    End Sub
    Public Sub DisplayItems()
        PanelSrch.Visible = False
        Me.RemoveAllControl()
        Me.Label5.Text = 1
        Dim nbrt As Integer = (Me.GetItemsCountInOneLine() * Me.GetItemsCountInOneCol())
        ' Me.FlowLayoutPanel1.Visible = False
        For ifs As Integer = 0 To nbrt - 1
            Try
                Me.AddItem(m_itemsList(ifs))
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Next
        '  Me.FlowLayoutPanel1.Visible = True
    End Sub
    Public Sub DisplayItems(ByVal num As Integer)
        PanelSrch.Visible = False
        Me.RemoveAllControl()
        Me.Label5.Text = num
        Dim nbrt As Integer = (Me.GetItemsCountInOneLine() * Me.GetItemsCountInOneCol())
        '  Me.FlowLayoutPanel1.Visible = False
        For ifs As Integer = ((num * nbrt) - nbrt) To (num * nbrt) - 1
            Try
                Me.AddItem(m_itemsList(ifs))
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Next
        ' Me.FlowLayoutPanel1.Visible = True
    End Sub
    Public Sub DisplayItems(ByVal num As Integer, ByVal strToFind As String)
        If Not m_itemsList Is Nothing Then
            Me.RemoveAllControl()
            'num = 1
            'nbrt = 8
            'cpt = 1
            Me.Label5.Text = num

            'num = 1
            'i = ((1 - 1) * 8) + 1 = 1
            'num = 2
            'i = ((2 - 1) * 8) + 1 = 9
            PanelSrch.Visible = True
            Dim cpt As Integer = 1
            Dim nbrt As Integer = (Me.GetItemsCountInOneLine() * Me.GetItemsCountInOneCol())
            Dim i As Integer = ((num - 1) * nbrt) + 1
            For ifs As Integer = 0 To m_itemsList.Count - 1
                Try
                    If m_itemsList(ifs).Name.ToLower.Contains(strToFind) = True Then
                        If cpt >= i AndAlso cpt <= nbrt + (i - 1) Then
                            cpt += 1
                            Me.AddItem(m_itemsList(ifs))
                        ElseIf cpt < i Then
                            cpt += 1
                        ElseIf cpt > nbrt Then
                            cpt += 1
                        End If

                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Next
            Label28.Text = cpt
        End If
        ' Me.FlowLayoutPanel1.Visible = True
    End Sub
    Public Sub AddItem(ByVal Itm As ShopItem)
        Me.Invoke(
            Sub()
                If Me.ClientSize <> Me.Size Then
                    Me.ClientSize = Me.Size
                End If
                Dim ctrl5 As Panel = KFPControlCloner.CloneCtrl(Me.PnlItem)
                number += 1
                Me.FlowLayoutPanel1.Controls.Add(ctrl5)
                ctrl5.Name = ctrl5.Name & "_" & number
                If Itm.Color = Nothing Then
                    Itm.Color = ItemBackColor
                End If
                ctrl5.BackColor = Itm.Color
                ctrl5.Show()
                Dim number2 As Integer = 1
                For Each ctrl2 As Control In Me.PnlItem.Controls
                    Dim ctrl As Control = KFPControlCloner.CloneCtrl(ctrl2)
                    If (ctrl.GetType() Is GetType(PictureBox)) Then
                        Try
                            Dim ctrlPic As PictureBox = KFPControlCloner.CloneCtrl(ctrl2)
                            number2 += 1
                            ctrl5.Controls.Add(ctrlPic)
                            ctrlPic.SetBounds(ctrlPic.Bounds.X, ctrlPic.Bounds.Y, ctrlPic.Bounds.Width, ctrlPic.Bounds.Height)

                            If (ctrlPic.Name.Equals("PicItem") = True) Then
                                If Itm.Icons Is Nothing Or Len(Itm.Icons) < 4 Then
                                    ctrlPic.SizeMode = PictureBoxSizeMode.StretchImage
                                Else
                                    ctrlPic.Image = Image.FromFile(Itm.Icons)
                                End If

                                AddHandler ctrlPic.MouseEnter, AddressOf ctrPic_MouseEnter
                                AddHandler ctrlPic.MouseLeave, AddressOf ctrPic_MouseLeave
                                AddHandler ctrlPic.DoubleClick, AddressOf ctrPic_DoubleClick
                                AddHandler ctrlPic.MouseUp, AddressOf ctrPic_MouseUp
                                'AddHandler ctrlPic.KeyUp, AddressOf ctrPic_KeyUp
                            End If
                            ctrlPic.Name = ctrl.Name & "_" & number
                            ctrlPic.Show()
                        Catch ex As Exception
                        End Try
                    Else
                        number2 += 1
                        ctrl5.Controls.Add(ctrl)
                        ctrl.SetBounds(ctrl.Bounds.X, ctrl.Bounds.Y, ctrl.Bounds.Width, ctrl.Bounds.Height)
                        If (ctrl.GetType() Is GetType(Label)) Then
                            If (ctrl.Name.Equals("LblName") = True) Then
                                ctrl.Text = Itm.Name ' AddHandler ctrl.MouseDoubleClick, AddressOf ctr_MouseDoubleClick
                                AddHandler ctrl.MouseUp, AddressOf ctrPic_MouseUp

                            End If
                        End If
                        If (ctrl.GetType() Is GetType(TextBox)) Then
                            Select Case True
                                Case ctrl.Name.Equals("TextBox2") = True
                                    ctrl.Text = Itm.CurrentPrice
                                    AddHandler ctrl.TextChanged, AddressOf ctrPic_TextChanged
                                    AddHandler ctrl.MouseUp, AddressOf ctrl_MouseUp
                                    AddHandler ctrl.KeyUp, AddressOf ctrl_KeyUp
                                    AddHandler ctrl.LostFocus, AddressOf ctr_LostFocus
                                Case ctrl.Name.Equals("TxtMiddlePrice") = True
                                    ctrl.Text = "N/C"
                            End Select
                            ' AddHandler ctrl.LostFocus, AddressOf ctr_LostFocus
                            'AddHandler ctrl.KeyUp, AddressOf ctr_KeyUp
                        End If
                        If (ctrl.GetType() Is GetType(NumericTextBox)) Then
                            Select Case True
                                Case ctrl.Name.Equals("TextBox2") = True
                                    ctrl.Text = Itm.CurrentPrice
                                    AddHandler ctrl.TextChanged, AddressOf ctrPic_TextChanged
                                    AddHandler ctrl.MouseUp, AddressOf ctrl_MouseUp
                                    AddHandler ctrl.KeyUp, AddressOf ctrl_KeyUp
                                    AddHandler ctrl.LostFocus, AddressOf ctr_LostFocus
                                Case ctrl.Name.Equals("TxtMiddlePrice") = True
                                    ctrl.Text = "N/C"
                            End Select
                            ' AddHandler ctrl.LostFocus, AddressOf ctr_LostFocus
                            'AddHandler ctrl.KeyUp, AddressOf ctr_KeyUp
                        End If
                        ctrl.Name = ctrl.Name & "_" & number
                        ctrl.Visible = True
                        ctrl.Show()
                    End If
                Next
                '  ctrl5.Show()
            End Sub)

    End Sub

    Private Sub ctrl_MouseUp(sender As Object, e As MouseEventArgs)
    

    End Sub
    Private Sub ctrPic_TextChanged(sender As Object, e As EventArgs)
        Try
            If sender.name = "TXTYourPrice" Then
                Exit Sub

            End If
            Dim id As Integer = Replace(Mid(sender.name, 10, 4), "_", "")
            Dim itmName As String = Me.FlowLayoutPanel1.Controls("PnlItem_" & id).Controls("LblName_" & id).Text
            Dim itmPrice As String = sender.Text
            Dim regex As Regex = New Regex("^.*[0-9]$")
            Dim match As Match = regex.Match(itmPrice.Trim)
            If match.Success = True Then
                Dim shpI As ShopItem = Me.m_itemsList.GetShopItemByName(itmName)
                If Not shpI Is Nothing Then
                    shpI.CurrentPrice = itmPrice.Trim
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub
    Private Sub ctr_LostFocus(sender As Object, e As EventArgs) Handles TextBox2.LostFocus
        Dim i As Integer = CInt(sender.Text)
        'sender.Visible = False
        Dim id As Integer = Mid(sender.name, InStrRev(sender.name, "_") + 1, 4)
        Dim shpI As ShopItem = Me.m_itemsList.GetShopItemByName(Me.FlowLayoutPanel1.Controls("PnlItem_" & id).Controls("LblName_" & id).Text)
        If Not shpI Is Nothing Then
            shpI.CurrentPrice = i
        End If
    End Sub
    Private Sub Label3_Click(sender As Object, e As EventArgs) Handles Label3.Click
        If CInt(Label5.Text) < CInt(LblPages.Text) Then
            Label5.Text = (CInt(Label5.Text) + 1)
            DisplayItems(Label5.Text)
        End If
    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click
        If CInt(Label5.Text) > 1 Then
            Label5.Text = (CInt(Label5.Text) - 1)
            DisplayItems(Label5.Text)
        End If
    End Sub
    Private Sub ctrPic_MouseEnter(sender As Object, e As EventArgs) Handles PicItem.MouseEnter

        'Dim FLW As Integer = Me.FlowLayoutPanel1.Width
        'Dim FLH As Integer = Me.FlowLayoutPanel1.Height
        'Dim parPosX As Integer = sender.parent.location.x
        'Dim parPosY As Integer = sender.parent.location.y
        'Dim posx As Integer = 0
        'Dim posy As Integer = 0

        'If parPosX > (FLW / 2) Then
        '    posx = ((sender.parent.location.x + sender.width / 2) - PnlToolTip.Width - 30)
        'Else
        '    posx = (sender.parent.location.x + sender.width / 2) + 30
        'End If
        'If parPosY > (FLH / 2) Then
        '    posy = (sender.parent.location.y - PnlToolTip.Height / 2 - 30)
        'Else
        '    posy = sender.parent.location.y + 70
        'End If
        'PnlToolTip.Location = New Point(posx, posy)
        'PnlToolTip.Visible = True
        PictureBox2.Image = sender.image

        Dim id As Integer = Mid(sender.name, 9, 4)
        Dim itmName As String = Me.FlowLayoutPanel1.Controls("PnlItem_" & id).Controls("LblName_" & id).Text
        Dim itm As ShopItem = Me.m_itemsList.GetShopItemByName(itmName)
        If Not itm Is Nothing Then
            LblinfoW.Text = itm.Weight
            LblitemNameInfo.Text = itm.Name

            LblInfoGroup.Text = itm.Group
            LblInfoRT.Text = itm.RepairTools
            LblInfoSN.Text = itm.MaxStack
            LblInfoRange.Text = itm.Range
            LblMaterial.Text = itm.Material
            KfP_TextBox1.Text = itm.CurrentPrice

        End If



    End Sub
    Private Sub ctrPic_MouseLeave(sender As Object, e As EventArgs) Handles PicItem.MouseLeave
        ' PnlToolTip.Visible = False
    End Sub
    Private Sub ctrPic_DoubleClick(sender As Object, e As EventArgs) Handles PicItem.DoubleClick
        Dim id As Integer = Mid(sender.name, 9, 4)
        Dim itmName As String = Me.FlowLayoutPanel1.Controls("PnlItem_" & id).Controls("LblName_" & id).Text
        Dim itmCount As String = Me.FlowLayoutPanel1.Controls("PnlItem_" & id).Controls("TxtNbrItem_" & id).Text
        Picitem_DoubleClick(itmName, itmCount)

    End Sub
    Private Sub ctrPic_MouseUp(sender As Object, e As EventArgs) Handles PicItem.MouseUp
        '  MsgBox(sender.parent.name)
    End Sub
    Private Sub Label6_TextChanged(sender As Object, e As EventArgs) Handles Label6.TextChanged
        LblPages.Text = Math.Ceiling(CInt(Label6.Text) / (Me.GetItemsCountInOneLine() * Me.GetItemsCountInOneCol()))
    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click
        Label5.Text = LblPages.Text
        DisplayItems(Label5.Text)
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click
        Label5.Text = "1"
        DisplayItems()
    End Sub

    Private Sub Label5_Click(sender As Object, e As EventArgs) Handles Label5.Click
        TextBox1.Text = Label5.Text
        TextBox1.Visible = True
    End Sub

    Private Sub TextBox1_KeyUp(sender As Object, e As KeyEventArgs) Handles TextBox1.KeyUp
        If e.KeyCode = Keys.Enter Then
            FlowLayoutPanel1.Focus()
        End If
    End Sub

    Private Sub TextBox1_LostFocus(sender As Object, e As EventArgs) Handles TextBox1.LostFocus
        Dim i As Integer = CInt(TextBox1.Text)
        If i >= 1 AndAlso i <= CInt(LblPages.Text) Then
            Label5.Text = TextBox1.Text
            TextBox1.Visible = False
            RaiseNewEvent(Label5.Text)
        End If
    End Sub
  
    Private Sub TextBox23_TextChanged(sender As Object, e As EventArgs) Handles TextBox23.TextChanged
        Dim val1 As String = TextBox23.Text
        If Len(val1) > 1 Then
            DisplayItems(1, val1)
        End If
    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.RemoveAllControl()
        Me.AddItem(Me.m_itemsList.GetShopItemByName(ComboBox1.Text)) '  = Me.m_IconsPath & ComboBox1.Text & ".png"
    End Sub
    'Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
    '    Dim random As New Random(), rndnbr As Integer

    '    '.Next permet de retourner un nombre aléatoire contenu dans la plage spécifiée entre parenthèses.
    '    rndnbr = random.Next(0, LVgift.Items.Count - 1)
    '    LVgift.Items(rndnbr).Checked = True
    '    rndnbr = random.Next(0, ComboBox1.Items.Count - 1)

    '    ComboBox1.SelectedIndex = (rndnbr)
    '    TextBox1.Text = 1
    '    TextBox23.Text = ComboBox1.Text
    '    Button19_Click(sender, e)
    'End Sub


    Private Sub PictureBox15_Click(sender As Object, e As EventArgs) Handles PictureBox15.Click
        DisplayItems()
    End Sub
    Private Sub ctrPic_MouseUp(sender As Object, e As MouseEventArgs) Handles PicItem.MouseUp
        Dim id As Integer = Mid(sender.name, 9, 4)
        Dim nbr As Integer = CInt(Me.FlowLayoutPanel1.Controls("PnlItem_" & id).Controls("TxtNbrItem_" & id).Text)
        nbr += 1
        Me.FlowLayoutPanel1.Controls("PnlItem_" & id).Controls("TxtNbrItem_" & id).Text = nbr
    End Sub
    Private Sub PnlToolTip_MouseWheel(sender As Object, e As MouseEventArgs) Handles PnlToolTip.MouseWheel
        If Panel4.Bounds.Contains(e.Location) Then
            Dim vScrollPosition As Integer = Panel4.VerticalScroll.Value
            vScrollPosition -= Math.Sign(e.Delta) * 23
            vScrollPosition = Math.Max(0, vScrollPosition)
            vScrollPosition = Math.Min(vScrollPosition, Me.VerticalScroll.Maximum)
            Panel4.AutoScrollPosition = New Point(Panel4.AutoScrollPosition.X, _
                                  vScrollPosition)
            Panel4.Invalidate()
        End If
    End Sub

    Private Sub PnltoolTipIn_MouseEnter(sender As Object, e As EventArgs) Handles PnltoolTipIn.MouseEnter
        PnlToolTip.Focus()
    End Sub

    Private Sub Label24_Click(sender As Object, e As EventArgs) Handles Label24.Click '>>
        Label21.Text = Label23.Text
        DisplayItems(Label21.Text, TextBox23.Text)
    End Sub

    Private Sub Label25_Click(sender As Object, e As EventArgs) Handles Label25.Click '>
        If CInt(Label21.Text) < Label23.Text Then
            Label21.Text = (CInt(Label21.Text) + 1)
            DisplayItems(Label21.Text, TextBox23.Text)
        End If
    End Sub

    Private Sub Label27_Click(sender As Object, e As EventArgs) Handles Label27.Click '<<
        Label21.Text = 1
        DisplayItems(1, TextBox23.Text)
    End Sub

    Private Sub Label26_Click(sender As Object, e As EventArgs) Handles Label26.Click '<
        If CInt(Label21.Text) > 1 Then
            Label21.Text = (CInt(Label21.Text) - 1)
            DisplayItems(Label21.Text, TextBox23.Text)
        End If
    End Sub
    Private Sub Label28_TextChanged(sender As Object, e As EventArgs) Handles Label28.TextChanged
        Label23.Text = Math.Ceiling(CInt(Label28.Text) / (Me.GetItemsCountInOneLine() * Me.GetItemsCountInOneCol()))
    End Sub
    Private Sub FlowLayoutPanel1_MouseEnter(sender As Object, e As EventArgs) Handles FlowLayoutPanel1.MouseEnter
        '      PnlToolTip.Visible = False
    End Sub



    Private Sub KfP_TextBox1_KeyUp(sender As Object, e As KeyEventArgs) Handles KfP_TextBox1.KeyUp

    End Sub

    Private Sub KfP_TextBox1_TextChanged_1(sender As Object, e As EventArgs) Handles KfP_TextBox1.TextChanged
        Try
            'If Not KfP_TextBox1.Text = "N/C" AndAlso Me.m_itemsList.Count > 0 Then
            '    If LblitemNameInfo.Text.ToLower.Equals("item infos") = False Then
            '        Dim shpI As ShopItem = Me.m_itemsList.GetShopItemByName(LblitemNameInfo.Text)
            '        If Not shpI Is Nothing Then
            '            shpI.CurrentPrice = KfP_TextBox1.Text
            '            For Each ctrl In Me.FlowLayoutPanel1.Controls
            '                For Each subctrl In ctrl.Controls
            '                    If Mid(subctrl.name, 1, 7).Equals("LblName") AndAlso Len(subctrl.name) > 1 Then
            '                        If subctrl.text.Equals(LblitemNameInfo.Text) = True Then
            '                            Dim id As Integer = Mid(subctrl.name, 9, 4)
            '                            Me.FlowLayoutPanel1.Controls("PnlItem_" & id).Controls("TXTYourPrice_" & id).Text = KfP_TextBox1.Text
            '                        End If
            '                    End If

            '                Next
            '            Next
            '            '("PnlItem_" & id).Controls("TxtNbrItem_" & id).Text = nbr
            '        End If
            '    End If
            'End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub ctrl_KeyUp(sender As Object, e As KeyEventArgs) Handles TextBox2.KeyUp
        If e.KeyCode = Keys.Enter Then
            FlowLayoutPanel1.Focus()
        End If
    End Sub

    Private Sub TextBox2_LostFocus(sender As Object, e As EventArgs) Handles TextBox2.LostFocus

    End Sub

    Private Sub TextBox2_MouseUp(sender As Object, e As MouseEventArgs) Handles TextBox2.MouseUp

    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged

    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub PicItem_Click(sender As Object, e As EventArgs) Handles PicItem.Click

    End Sub

    Private Sub FlowLayoutPanel1_Paint(sender As Object, e As PaintEventArgs) Handles FlowLayoutPanel1.Paint

    End Sub

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        Process.Start("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=XLGJTYP2A6FUQ", Nothing)
    End Sub
End Class
